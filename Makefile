up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear landpro-clear docker-pull docker-build docker-up landpro-init
require: landpro-composer-require
cc: cache-clear
test: landpro-test

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

landpro-clear:
	docker run --rm -v ${PWD}/landpro:/app --workdir=/app alpine rm -f .ready

landpro-init: landpro-composer-install landpro-wait-db landpro-migrations landpro-ready

landpro-wait-db:
	until docker-compose exec -T land-postgres pg_isready --timeout=0 --dbname=landpro ; do sleep 1 ; done

landpro-migrations:
	docker-compose run --rm php-cli php bin/console doctrine:migrations:migrate --no-interaction

landpro-ready:
	docker run --rm -v ${PWD}/landpro:/app --workdir=/app alpine touch .ready

landpro-composer-install:
	docker-compose run --rm php-cli composer install

landpro-composer-require:
	docker-compose run --rm php-cli composer require $(app)

cache-clear:
	docker-compose run --rm php-cli php bin/console cache:clear

landpro-test:
	docker-compose run --rm php-cli php vendor/bin/phpunit