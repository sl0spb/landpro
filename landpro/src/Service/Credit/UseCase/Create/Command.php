<?php

declare(strict_types=1);

namespace App\Service\Credit\UseCase\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{

    /**
     * @Assert\NotBlank()
     */
    public int $sum = 0;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min="0", max="100")
     */
    public float $percent = 0;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min="1")
     */
    public int $duration = 1;

}

