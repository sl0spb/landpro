<?php

declare(strict_types=1);

namespace App\Service\Credit\UseCase\Create;

use App\Service\Flusher;

class Handler
{
    public const URL = 'api/credit';
    private Flusher $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    public function handle(Command $command)
    {

        return \ApiService::getContent(self::URL, 'POST', serialize($command));

    }
}
