<?php

declare(strict_types=1);

namespace App\Service\Credit\UseCase\Calculate;

use App\Entity\Credit;
use App\ReadModel\Credit\CreditFetcher;
use App\Service\Credit\UseCase\Create\Command;
use App\Service\Flusher;

class Handler
{
    public array $monthsName = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    private Flusher $flusher;
    private CreditFetcher $fetcher;

    public function __construct(Flusher $flusher, CreditFetcher $fetcher)
    {
        $this->flusher = $flusher;
        $this->fetcher = $fetcher;
    }

    public function handle(Command $command)
    {
        if ($data = $this->fetcher->findData($command)) {
            return json_decode($data,true);
        }

        $credit = Credit::fromCommand($command);

        $sum = $command->sum * 100;
        $percent = $command->percent;
        $duration = $command->duration;

        $interest = $percent / 100;

        $m = floor((sqrt((($interest + 2) ** 2) + 8 * $interest * $duration) - ($interest + 2)) / (2 * $interest));

        $m2 = (2 * $duration + $m * ($m + 1) * $interest) / (2 * ($m + 1) * $interest + 2);
        $x = ceil($sum / $m2);

        $result = [];
        $debt = $sum;
        $np = 0;
        $monthCount = 0;
        $year = 0;
        for ($i = 0; $i < $duration; $i++) {
            if ($monthCount >= 12) {
                $monthCount = 0;
                $year++;
            }
            $month = $this->monthsName[$monthCount++];
            $p = ceil($debt * $percent / 100);
            $debt = $debt - $x;
            if ($debt < 0) {
                $debt = 0;
            }
            $np += $p;
            if (!$debt && !$p) {
                $np -= $x;
            }
            if ($np < 0) {
                $np = 0;
            }

            $result[$year][] = [
                'month'       => $month,
                'payment'     => $x / 100,
                'debt'        => $debt / 100,
                'debtPercent' => $np / 100,
                'percents'    => $p / 100,
            ];
            if ($x > $debt && $debt !== 0) {
                $ab = $x - $debt;
                $np -= $ab;
            }
        }

        $credit->setData(json_encode($result));

        $this->fetcher->add($credit);
        $this->flusher->flush();

        return $result;
    }
}