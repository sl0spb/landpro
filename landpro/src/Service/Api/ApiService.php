<?php

declare(strict_types=1);

class ApiService
{
    public static function getContent(string $url, string $type = 'GET', string $data = '')
    {
        $curl = curl_init();

        $url = 'host.docker.internal/' . $url;
        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 300,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => $type,
        ]);

        $headers = [
            'Cache-Control: no-cache',
        ];

        if ($type == 'POST') {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return null;
        }

        return json_decode($response);
    }
}