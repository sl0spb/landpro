<?php

declare(strict_types=1);

namespace App\ReadModel\Credit;

use App\Entity\Credit;
use App\Service\Credit\UseCase\Create\Command;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;

class CreditFetcher
{
    private Connection $connection;
    private EntityManagerInterface $em;

    public function __construct(Connection $connection, EntityManagerInterface $em)
    {
        $this->connection = $connection;
        $this->em = $em;
    }

    public function findData(Command $command)
    {
        $stmt = $this
            ->connection->createQueryBuilder()
            ->select(
                'c.data',
            )
            ->from('credits', 'c')
            ->andWhere('c.sum = :sum')
            ->andWhere('c.percent = :percent')
            ->andWhere('c.duration = :duration')
            ->setParameter(':sum', $command->sum)
            ->setParameter(':percent', $command->percent)
            ->setParameter(':duration', $command->duration)
            ->execute();
        $data = $stmt->fetchColumn();

        return $data ? json_decode($data, true) : null;
    }

    public function add(Credit $credit): void
    {
        $this->em->persist($credit);
    }
}