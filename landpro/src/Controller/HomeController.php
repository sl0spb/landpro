<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Credit\UseCase\Create;

class HomeController extends AbstractController
{
    private ErrorHandler $errors;

    public function __construct(ErrorHandler $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @Route("/", name="home")
     * @param \Symfony\Component\HttpFoundation\Request  $request
     *
     * @param \App\Service\Credit\UseCase\Create\Handler $handler
     *
     * @return Response
     */
    public function index(Request $request, Create\Handler $handler): Response
    {

        $command = new Create\Command();
        $result = [];
        $form = $this->createForm(Create\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $result = $handler->handle($command);
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('app/home.html.twig', [
            'form'   => $form->createView(),
            'result' => $result,
        ]);
    }

}