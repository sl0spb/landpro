<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Service\Credit\UseCase\Calculate\Handler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreditController extends AbstractController
{

    /**
     * @Route("/credit", name="credit", methods={"POST"})
     * @param \Symfony\Component\HttpFoundation\Request     $request
     *
     * @param \App\Service\Credit\UseCase\Calculate\Handler $handler
     *
     * @return Response
     */
    public function home(Request $request, Handler $handler): Response
    {

        $command = unserialize($request->getContent());
        return $this->json($handler->handle($command));

    }

    /**
     * @Route("/test", name="test", methods={"GET"})
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return Response
     */
    public function test(Request $request)
    {
        return $this->json([
            'name' => 'JSON API',
        ]);
    }
}