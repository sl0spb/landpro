<?php

declare(strict_types=1);

namespace App\Entity;

use App\Service\Credit\UseCase\Create\Command;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="credits", indexes={
 *     @ORM\Index(columns={"sum", "percent", "duration"})
 * })
 */
class Credit
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer")
     */
    private int $sum;

    /**
     * @ORM\Column(type="float")
     */
    private float $percent;

    /**
     * @ORM\Column(type="integer")
     */
    private int $duration;

    /**
     * @ORM\Column(type="json")
     */
    private string $data;

    /**
     * Credit constructor.
     *
     * @param int   $sum
     * @param float $percent
     * @param int   $duration
     */
    public function __construct(int $sum, float $percent, int $duration)
    {
        $this->sum = $sum;
        $this->percent = $percent;
        $this->duration = $duration;
    }

    public static function fromCommand(Command $command)
    {
        return new self($command->sum, $command->percent, $command->duration);
    }

    /**
     * @return int
     */
    public function getSum(): int
    {
        return $this->sum;
    }

    /**
     * @param int $sum
     */
    public function setSum(int $sum): void
    {
        $this->sum = $sum;
    }

    /**
     * @return float
     */
    public function getPercent(): float
    {
        return $this->percent;
    }

    /**
     * @param float $percent
     */
    public function setPercent(float $percent): void
    {
        $this->percent = $percent;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }


    public function setData($data): void
    {
        $this->data = $data;
    }

}